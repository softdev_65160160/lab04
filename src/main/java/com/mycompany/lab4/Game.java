/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Game {
    private Player player1,player2;
    private Table table;
    
    public Game(){
        this.player1 = new Player('O');
        this.player2 = new Player('X');   
    }

    public void newGame(){
        this.table = new Table(player1,player2);       
    }
    
    public void play() {
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                showWin();
                showInfo();
                newGame();
            }
            if (table.checkDraw()) {
                showTable();
                showDraw();
                showInfo();
                newGame();
            }
            table.switchPlayer();
        }
        
    }
      
    private void showWelcome() {
        System.out.println("Welcome to XO game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int row = 0;row <3;row++){
            for(int col =0;col<3;col++){
                System.out.print(t[row][col]+" ");
            }
            System.out.println();
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        
        while(true){
            System.out.print("Please input row col: ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if(table.setRowCol(row,col)){
                break;
            }           
        }
        
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    private void showWin() {
        if (table.checkWin()){
            System.out.println(table.getCurrentPlayer().getSymbol()+" is a winner!");
        }
    }

    private void showDraw() {
        if (table.checkDraw()){
            System.out.println("Game is a draw!");
        }
    }
      
}

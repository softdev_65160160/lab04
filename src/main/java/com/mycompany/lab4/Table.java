/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author Lenovo
 */
public class Table {
    
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;  
    private int row,col;
    
    public  Table(Player player1,Player player2){
        this.player1=player1;
        this.player2=player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public boolean setRowCol(int row,int col){
        if (table[row-1][col-1]=='-'){
            table[row-1][col-1]=currentPlayer.getSymbol();
            this.row= row;
            this.col= col;
            return true;
        }
        return false;
    }
    
    public void switchPlayer(){
        if (currentPlayer==player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
    }
    
    public boolean checkWin(){
        if (checkRow()) {
            saveWin();
            return true;
        }else if(checkCol()){
            saveWin();
            return true;
        }else if(checkX1()){
            saveWin();
            return true;
        }else if(checkX2()){
            saveWin();
            return true;
        }else{
            return false;
        }
    }
    
    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkDraw() {      
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if (table[i][j]!=player1.getSymbol()&&table[i][j]!=player2.getSymbol()){
                    return false;
                }
            }
        }
        saveDraw();
        return true;
    }
    
    private void saveWin() {
        if (player1 == getCurrentPlayer()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();

        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }
       
}
